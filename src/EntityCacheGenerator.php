<?php

namespace Drupal\entity_cache_generator;

/**
 * Generates entity cache for specified entity types.
 */
class EntityCacheGenerator {

  /**
   * Generates entity cache for specified entity types.
   *
   * @param array $entity_types
   *   Entity types.
   */
  public function generate(array $entity_types): void {
    foreach ($entity_types as $entity_type) {
      $this->generateEntityCache($entity_type);
    }
  }

  /**
   * Generates entity cache for specified entity type.
   *
   * @param string $entity_type
   *   Entity type.
   */
  private function generateEntityCache(string $entity_type): void {
    $entity_type_manager = \Drupal::entityTypeManager();
    $entity_storage = $entity_type_manager->getStorage($entity_type);
    $entity_ids = $entity_storage->getQuery()->accessCheck(FALSE)->execute();
    $entity_storage->loadMultiple($entity_ids);
  }
}