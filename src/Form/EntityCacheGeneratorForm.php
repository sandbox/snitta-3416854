<?php declare(strict_types = 1);

namespace Drupal\entity_cache_generator\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a Entity cache generator form.
 */
final class EntityCacheGeneratorForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'entity_cache_generator_entity_cache_generator';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $form['message'] = [
      '#markup' => $this->t('Choose entity types to generate cache.'),
    ];

    $form['entity_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Entity types'),
      '#options' => [],
      '#default_value' => \Drupal::config('entity_cache_generator.settings')->get('entity_types') ? : [],
      '#required' => TRUE,
    ];

    // Get existing content entity types.
    $entity_types = \Drupal::entityTypeManager()->getDefinitions();
    $content_entity_types = array_filter($entity_types, function ($entity_type) {
      return $entity_type instanceof \Drupal\Core\Entity\ContentEntityTypeInterface;
    });
    foreach ($content_entity_types as $entity_type) {
      $form['entity_types']['#options'][$entity_type->id()] = $entity_type->getLabel();
    }

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Submit'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->messenger()->addStatus($this->t('The message has been sent.'));
    $entity_types = array_filter($form_state->getValue('entity_types'));

    // Save as default value.
    $config = \Drupal::configFactory()->getEditable('entity_cache_generator.settings');
    $config->set('entity_types', $entity_types)->save();

    $batch = \Drupal::service('entity_cache_generator.batch')->getBatch($entity_types);
    dpm($batch);
    batch_set($batch);
  }

}
