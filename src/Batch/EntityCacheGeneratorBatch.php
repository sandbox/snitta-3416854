<?php

namespace Drupal\entity_cache_generator\Batch;

use Drupal\Core\Batch\BatchBuilder;

class EntityCacheGeneratorBatch {

  const BATCH_SIZE = 100;

  public function getBatch($entity_types) {
    $batch_builder = (new BatchBuilder())
      ->setTitle('Generating entity cache')
      ->setFinishCallback([$this, 'finishCallback'])
      ->setInitMessage('Starting...')
      ->setProgressMessage('Processed @current out of @total batches.')
      ->setErrorMessage('An error occurred during processing');

    foreach ($entity_types as $entity_type) {
      $storage = \Drupal::entityTypeManager()->getStorage($entity_type);
      $entity_ids = $storage->getQuery()->accessCheck(FALSE)->execute();
      $chunks = array_chunk($entity_ids, self::BATCH_SIZE);
      foreach ($chunks as $chunk) {
        $batch_builder->addOperation([$this, 'generateEntityCache'], [$entity_type, $chunk]);
      }
    }

    return $batch_builder->toArray();
  }

  public function generateEntityCache($entity_type, $entity_ids, &$context) {
    $storage = \Drupal::entityTypeManager()->getStorage($entity_type);
    $storage->loadMultiple($entity_ids);
  }

  public function finishCallback($success, $results, $operations) {
    if ($success) {
      //drupal_set_message('Entity cache generated.');
    }
    else {
      //drupal_set_message('An error occurred during processing.', 'error');
    }
  }
}