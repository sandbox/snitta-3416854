## Usage
This module provides several ways to generate cache for entities.

### Generate cache from GUI
1. Open /admin/entity-cache-generator/generate
2. Select entity type
3. Submit from

### Generate cache in php code
```php
$entity_types = ['node', 'taxonomy_term'];
$entityCacheGenerator = \Drupal::service('entity_cache_generator.generator')->generate($entity_types);
```